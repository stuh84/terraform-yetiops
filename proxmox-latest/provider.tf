terraform {
  required_providers {
    proxmox = {
      source = "Telmate/proxmox"
      version = "2.7.1"
    }
  }
}

provider "proxmox" {
  pm_api_url = "https://192.168.30.251:8006/api2/json"
  pm_user = "terraform-prov@pve"
  pm_tls_insecure = true
}
