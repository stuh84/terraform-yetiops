data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-arm64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

data "aws_vpc" "default" {
  default = true
}

resource "aws_key_pair" "yetiops-aws-prom" {
  key_name = "yetiops-aws-prom"
  public_key = file("~/.ssh/id_rsa.pub") 
}

resource "aws_instance" "yetiops-aws-prom" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = "t4g.micro"
  user_data     = data.template_file.ubuntu.template

  key_name = aws_key_pair.yetiops-aws-prom.key_name

  vpc_security_group_ids = [
    aws_security_group.yetiops-aws-prom.id
  ]

  tags = {
    Name = "yetiops-aws-prom"
    prometheus = "true"
    node_exporter = "true"
  }
}

resource "aws_security_group" "yetiops-aws-prom" {
  name        = "yetiops-aws-prom"
  description = "AWS Security Group for yetiops-aws-prom"
  vpc_id      = data.aws_vpc.default.id 

  tags = {
    Name = "yetiops-aws-prom"
  }
}

resource "aws_security_group_rule" "ingress_ssh_in" {
  type              = "ingress"
  to_port           = 22
  protocol          = "tcp"
  from_port         = 22
  cidr_blocks       = [
    "$MYIP/32"
  ]
  security_group_id = aws_security_group.yetiops-aws-prom.id 
}

resource "aws_security_group_rule" "ingress_node_exporter_in" {
  type              = "ingress"
  to_port           = 9100 
  protocol          = "tcp"
  from_port         = 9100
  cidr_blocks       = [
    "$MYIP/32"
  ]
  security_group_id = aws_security_group.yetiops-aws-prom.id 
}

resource "aws_security_group_rule" "egress_allow_all" {
  type              = "egress"
  to_port           = 0
  protocol          = "-1"
  from_port         = 0
  cidr_blocks       = [
    "0.0.0.0/0"
  ]
  security_group_id = aws_security_group.yetiops-aws-prom.id 
}
