terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=2.46.0"
    }
    google = {
      source  = "hashicorp/google"
      version = "=3.62.0"
    }
    digitalocean= {
      source  = "digitalocean/digitalocean"
      version = "=2.7.0"
    }
  }
}

# AWS Provider
provider "aws" {
  region  = "eu-west-2"
}

# Azure Provider
provider "azurerm" {
  subscription_id = "$SUBSCRIPTION_ID"
  features {}
}

# GCP Provider
provider "google" {
  credentials = file("~/.gcloud/account.json")
  project = "yetiops-blog"
  region  = "europe-west3"
  zone    = "europe-west3-c"
}

# Digital Ocean Provider

provider "digitalocean" {
}
