resource "digitalocean_droplet" "yetiops-prom-vm" {
  image              = "ubuntu-20-04-x64"
  name               = "yetiops-prom-vm"
  region             = "fra1"
  size               = "s-1vcpu-1gb"
  ssh_keys           = [digitalocean_ssh_key.yetiops-ssh-key.fingerprint]

  tags = [
    digitalocean_tag.prometheus.id,
    digitalocean_tag.node_exporter.id
  ]

  user_data          = data.template_file.ubuntu.template

}

resource "digitalocean_tag" "prometheus" {
  name = "prometheus"
}

resource "digitalocean_tag" "node_exporter" {
  name = "node_exporter"
}

resource "digitalocean_ssh_key" "yetiops-ssh-key" {
  name       = "MBP SSH Key"
  public_key = file("~/.ssh/id_ed25519.pub")
}

resource "digitalocean_firewall" "yetiops-prom-vm" {
  name = "yetiops-prom-vm"

  droplet_ids = [digitalocean_droplet.yetiops-prom-vm.id]

  inbound_rule {
    protocol         = "tcp"
    port_range       = "22"
    source_addresses = ["$MYIP/32"]
  }

  inbound_rule {
    protocol         = "tcp"
    port_range       = "9100"
    source_addresses = ["$MYIP/32"]
  }

  inbound_rule {
    protocol         = "icmp"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol              = "icmp"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol              = "tcp"
    port_range            = "1-65535"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol              = "udp"
    port_range            = "1-65535"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }
}
