# GCP VM

resource "google_compute_instance" "yetiops-gcp-prom" {
  name         = "yetiops-gcp-prom"
  machine_type = "f1-micro"

  boot_disk {
    initialize_params {
      image = "ubuntu-2004-lts"
    }
  }

  network_interface {
    network       = google_compute_network.yetiops-gcp-prom-net.name 
    access_config {
    }
  }

  labels = {
    "prometheus" = "true"
    "node_exporter" = "true"
  }

  metadata = {
    enable-oslogin = "TRUE"
    user-data = data.template_cloudinit_config.ubuntu.rendered
  }  
}

# GCP Firewall

resource "google_compute_firewall" "yetiops-gcp-prom-fw-icmp" {
  name    = "yetiops-gcp-prom-fw-icmp"
  network = google_compute_network.yetiops-gcp-prom-net.name

  allow {
    protocol = "icmp"
  }

}

resource "google_compute_firewall" "yetiops-gcp-prom-fw-ssh" {
  name    = "yetiops-gcp-prom-ssh"
  network = google_compute_network.yetiops-gcp-prom-net.name
  direction = "INGRESS"
  source_ranges = ["$MYIP/32"]

  allow {
    protocol = "tcp"
    ports = ["22"]
  }
}

resource "google_compute_firewall" "yetiops-gcp-prom-fw-node-exporter" {
  name    = "yetiops-gcp-prom-node-exporter"
  network = google_compute_network.yetiops-gcp-prom-net.name
  direction = "INGRESS"
  source_ranges = ["$MYIP/32"]

  allow {
    protocol = "tcp"
    ports = ["9100"]
  }
}

# GCP VPC Network

resource "google_compute_network" "yetiops-gcp-prom-net" {
  name = "yetiops-gcp-prom-net"
}


# OS Login SSH Key

data "google_client_openid_userinfo" "me" {
}

resource "google_os_login_ssh_public_key" "cache" {
  user =  data.google_client_openid_userinfo.me.email
  key = file("~/.ssh/id_ed25519.pub")
}

resource "google_project_iam_member" "role-binding" {
  project = "yetiops-blog"
  role    = "roles/compute.osAdminLogin"
  member  = "user:yetiops.gcp@gmail.com" 
}
