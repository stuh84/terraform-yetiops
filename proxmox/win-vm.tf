# Source the Cloud Init Config file
data "template_file" "cloud_init_win2k19_vm-01" {
  template  = "${file("${path.module}/files/cloud_init_win2k19.cloud_config")}"

  vars = {
    hostname = "win2k19-vm-01"
    domain = "yetiops.lab"
  }
}

# Create a local copy of the file, to transfer to Proxmox
resource "local_file" "cloud_init_win2k19_vm-01" {
  content   = data.template_file.cloud_init_win2k19_vm-01.rendered
  filename  = "${path.module}/files/user_data_cloud_init_win2k19_vm-01.cfg"
}

# Transfer the file to the Proxmox Host
resource "null_resource" "cloud_init_win2k19_vm-01" {
  connection {
    type    = "ssh"
    user    = "root"
    private_key = file("~/.ssh/id_rsa")
    host    = "pve-01.yetiops.lab"
  }

  provisioner "file" {
    source       = local_file.cloud_init_win2k19_vm-01.filename
    destination  = "/var/lib/vz/snippets/cloud_init_win2k19_vm-01.yml"
  }
}


# Create the VM
resource "proxmox_vm_qemu" "win2k19-vm" {
  ## Wait for the cloud-config file to exist

    depends_on = [
    null_resource.cloud_init_win2k19_vm-01
  ]
  
  name = "win2k19-vm-01"
  target_node = "pve-01"
  
  # Clone from debian-cloudinit template
  clone = "win2k19-cloudinit"
  #os_type = "cloud-init"

  # Cloud init options 
  cicustom = "user=local:snippets/cloud_init_win2k19_vm-01.yml"
  ipconfig0 = "ip=dhcp"

  memory       = 2048
  agent        = 1

  # Set the boot disk paramters 
  bootdisk = "scsi0"
  scsihw       = "virtio-scsi-pci"

  disk {
    id              = 0
    size            = 20
    type            = "scsi"
    storage         = "local-lvm"
    storage_type    = "lvm"
    iothread        = true
  }

  # Set the network 
  network {
    id = 0
    model = "virtio"
    bridge = "vmbr0"
  }

  # Ignore changes to the network
  ## MAC address is generated on every apply, causing
  ## TF to think this needs to be rebuilt on every apply
  lifecycle {
     ignore_changes = [
       network
     ]
  }
}
