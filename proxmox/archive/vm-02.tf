# Source the Cloud Init Config file
data "template_file" "cloud_init_fedora_vm-02" {
  template  = "${file("${path.module}/files/cloud_init_fedora.cloud_config")}"

  vars = {
    ssh_key = file("~/.ssh/id_rsa.pub")
    hostname = "vm-02"
    domain = "yetiops.lab"
  }
}

# Create a local copy of the file, to transfer to Proxmox
resource "local_file" "cloud_init_fedora_vm-02" {
  content   = data.template_file.cloud_init_fedora_vm-02.rendered
  filename  = "${path.module}/files/user_data_cloud_init_fedora_vm-02.cfg"
}

# Transfer the file to the Proxmox Host
resource "null_resource" "cloud_init_fedora_vm-02" {
  connection {
    type    = "ssh"
    user    = "root"
    private_key = file("~/.ssh/id_rsa")
    host    = "10.15.31.7"
  }

  provisioner "file" {
    source       = local_file.cloud_init_fedora_vm-02.filename
    destination  = "/var/lib/vz/snippets/cloud_init_fedora_vm-02.yml"
  }
}


# Create the VM
resource "proxmox_vm_qemu" "vm-02" {
  ## Wait for the cloud-config file to exist

  depends_on = [
    null_resource.cloud_init_fedora_vm-02
  ]
  
  name = "vm-02"
  target_node = "pve-01"
  
  # Clone from debian-cloudinit template
  clone = "fedora-cloudinit"
  os_type = "cloud-init"

  # Cloud init options 
  cicustom = "user=local:snippets/cloud_init_fedora_vm-02.yml"
  ipconfig0 = "ip=10.15.31.97/24,gw=10.15.31.253"

  memory       = 1024
  agent        = 1

  # Set the boot disk paramters 
  bootdisk = "scsi0"
  scsihw       = "virtio-scsi-pci"

  disk {
    id              = 0
    size            = 10
    type            = "scsi"
    storage         = "local-lvm"
    storage_type    = "lvm"
    iothread        = true
  }

  # Set the network 
  network {
    id = 0
    model = "virtio"
    bridge = "vmbr0"
  }

  # Ignore changes to the network
  ## MAC address is generated on every apply, causing
  ## TF to think this needs to be rebuilt on every apply
  lifecycle {
     ignore_changes = [
       network
     ]
  }
}
