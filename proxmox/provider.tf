provider "proxmox" {
  pm_api_url = "https://10.15.31.7:8006/api2/json"
  pm_user = "terraform-pve@pve"
  pm_tls_insecure = true
  pm_timeout = 600
}
