# Create the generic auth backend - User and password
resource "vault_auth_backend" "approle" {
  type = "approle"
}

# KV Path Mount
resource "vault_mount" "secret-kv" {
  path        = "secret"
  type        = "kv"
  options = {
    "version" = "2"
  }
}

# Create a policy that can only write the kubeadm token
resource "vault_policy" "kubeadm_token_write" {
    name = "kubeadm_token_write"

    policy = <<EOT
path "secret/kubeadm_token" {
  capabilities = ["update", "create"]
}
path "secret/data/kubeadm_token" {
  capabilities = ["update", "create"]
}
EOT
}

# Create a policy that can only read the kubeadm token
resource "vault_policy" "kubeadm_token_read" {
    name = "kubeadm_token_read"

    policy = <<EOT
path "secret/kubeadm_token" {
  capabilities = ["read"]
}
path "secret/data/kubeadm_token" {
  capabilities = ["read"]
}
EOT
}

# KubeADM Read User
resource "vault_approle_auth_backend_role" "kubeadm_read" {
  backend   = vault_auth_backend.approle.path
  role_name = "kubeadm-read"
  token_policies  = ["kubeadm_token_read"]
}

resource "vault_approle_auth_backend_role_secret_id" "kubeadm_read" {
  backend   = vault_auth_backend.approle.path
  role_name = vault_approle_auth_backend_role.kubeadm_read.role_name
}

# KubeADM Write User
resource "vault_approle_auth_backend_role" "kubeadm_write" {
  backend   = vault_auth_backend.approle.path
  role_name = "kubeadm-write"
  token_policies  = ["kubeadm_token_write"]
}

resource "vault_approle_auth_backend_role_secret_id" "kubeadm_write" {
  backend   = vault_auth_backend.approle.path
  role_name = vault_approle_auth_backend_role.kubeadm_write.role_name
}
