resource "libvirt_pool" "libvirt-default-pool" {
  name = "kubernetes"
  type = "dir"
  path = var.libvirt_disk_path
}
