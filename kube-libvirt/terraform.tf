# instance the provider
provider "libvirt" {
  uri = "qemu:///system"
}

provider "vault" {
  address = "https://10.15.32.240:8200"
  skip_tls_verify = "true"
}
