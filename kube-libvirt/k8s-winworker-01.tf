resource "libvirt_volume" "k8s-winworker-01" {
  name = "k8s-winworker-01t"
  pool = libvirt_pool.libvirt-default-pool.name
  source = "/var/lib/libvirt/images/win2019-kube.qcow2" 
  format = "qcow2"
}

data "template_file" "k8s-winworker-01" {
  template = file("${path.module}/cloud-init/windows-workers.cfg")
  vars = {
    VAULT_KR_ROLE_ID = vault_approle_auth_backend_role.kubeadm_read.role_id
    VAULT_KR_SECRET_ID = vault_approle_auth_backend_role_secret_id.kubeadm_read.secret_id
    HOSTNAME = "k8s-winworker-01"
  }
}

resource "libvirt_cloudinit_disk" "k8s-winworker-01" {
  name           = "k8s-winworker-01.iso"
  user_data      = data.template_file.k8s-winworker-01.rendered
  meta_data      = jsonencode({
    "instance-id": random_id.instance_id.hex,
  })
  pool           = libvirt_pool.libvirt-default-pool.name
}

resource "random_id" "instance_id" {
  byte_length = 10
}

resource "libvirt_domain" "k8s-winworker-01" {
  name   = "k8s-winworker-01"
  memory = "4096"
  vcpu   = 2

  cloudinit = libvirt_cloudinit_disk.k8s-winworker-01.id

  network_interface {
    network_name = "mgmt-bridge"
    mac          = "52:54:00:FE:80:07"
  }


  # IMPORTANT: this is a known bug on cloud images, since they expect a console
  # we need to pass it
  # https://bugs.launchpad.net/cloud-images/+bug/1573095
  console {
    type        = "pty"
    target_port = "0"
    target_type = "serial"
  }

  console {
    type        = "pty"
    target_type = "virtio"
    target_port = "1"
  }

  disk {
    volume_id = libvirt_volume.k8s-winworker-01.id
  }

  graphics {
    type        = "vnc"
    listen_type = "address"
    autoport    = true
  }
}
