resource "libvirt_pool" "libvirt-default-pool" {
  name = "kubernetes"
  type = "dir"
  path = var.libvirt_disk_path
}


resource "libvirt_volume" "win2019-kube" {
  name = "win2019-kube-test"
  pool = libvirt_pool.libvirt-default-pool.name
  source = "/var/lib/libvirt/images/win2019-kube.qcow2" 
  format = "qcow2"
}

data "template_file" "user_data" {
  template = file("${path.module}/win_cloud_init.cfg")
}

resource "libvirt_cloudinit_disk" "commoninit" {
  name           = "win_commoninit.iso"
  user_data      = data.template_file.user_data.rendered
  meta_data      = jsonencode({
    "instance-id": random_id.instance_id.hex,
  })
  pool           = libvirt_pool.libvirt-default-pool.name
}

resource "random_id" "instance_id" {
  byte_length = 10
}

resource "libvirt_domain" "win2019-kube" {
  name   = "win2019-kube"
  memory = "4096"
  vcpu   = 2

  cloudinit = libvirt_cloudinit_disk.commoninit.id

  network_interface {
    network_name = "default"
  }

  # IMPORTANT: this is a known bug on cloud images, since they expect a console
  # we need to pass it
  # https://bugs.launchpad.net/cloud-images/+bug/1573095
  console {
    type        = "pty"
    target_port = "0"
    target_type = "serial"
  }

  console {
    type        = "pty"
    target_type = "virtio"
    target_port = "1"
  }

  disk {
    volume_id = libvirt_volume.win2019-kube.id
  }

  graphics {
    type        = "vnc"
    listen_type = "address"
    autoport    = true
  }
}
