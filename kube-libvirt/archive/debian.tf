resource "libvirt_pool" "libvirt-default-pool" {
  name = "kubernetes"
  type = "dir"
  path = var.libvirt_disk_path
}


resource "libvirt_volume" "k8s-cp-01" {
  name = "k8s-cp-01"
  pool = libvirt_pool.libvirt-default-pool.name
  source = "/var/lib/libvirt/images/debian10-kube.qcow2" 
  format = "qcow2"
}

data "template_file" "user_data" {
  template = file("${path.module}/cloud-init/debian-primary.cfg")
  vars = {
    SSH_KEY = file("~/.ssh/id_rsa.pub")
    VAULT_KW_ROLE_ID = vault_approle_auth_backend_role.kubeadm_write.role_id
    VAULT_KW_SECRET_ID = vault_approle_auth_backend_role_secret_id.kubeadm_write.secret_id
    HOSTNAME = "k8s-cp-01"
  }
}

resource "libvirt_cloudinit_disk" "commoninit" {
  name           = "commoninit.iso"
  user_data      = data.template_file.user_data.rendered
  pool           = libvirt_pool.libvirt-default-pool.name
}

resource "libvirt_domain" "k8s-cp-01" {
  name   = "k8s-cp-01"
  memory = "4096"
  vcpu   = 2

  cloudinit = libvirt_cloudinit_disk.commoninit.id

  network_interface {
    network_name = "mgmt-bridge"
    mac          = "52:54:00:FE:80:01"
  }

  # IMPORTANT: this is a known bug on cloud images, since they expect a console
  # we need to pass it
  # https://bugs.launchpad.net/cloud-images/+bug/1573095
  console {
    type        = "pty"
    target_port = "0"
    target_type = "serial"
  }

  console {
    type        = "pty"
    target_type = "virtio"
    target_port = "1"
  }

  disk {
    volume_id = libvirt_volume.k8s-cp-01.id
  }

  graphics {
    type        = "vnc"
    listen_type = "address"
    autoport    = true
  }
}
