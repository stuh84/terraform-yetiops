# AWS Provider
provider "aws" {
  region  = "eu-west-2"
}

# Azure Provider
provider "azurerm" {
  subscription_id = "$SUBSCRIPTION_ID"
  features {}
}

provider "azuread" {
}

# GCP Provider
provider "google" {
  credentials = file("~/.gcloud/account.json")
  project = "yetiops-blog"
  region  = "europe-west3"
  zone    = "europe-west3-c"
}

