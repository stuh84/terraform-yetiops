data "azurerm_subscription" "current" {
}

resource "azuread_application" "prometheus-sd-user" {
  name                              = "prometheus-sd-user"
  oauth2_allow_implicit_flow        = true
  available_to_other_tenants        = false
  type                              = "webapp/api"
}

resource "azuread_service_principal" "prometheus-sd-user" {
  application_id                    = azuread_application.prometheus-sd-user.application_id 
  app_role_assignment_required      = false
}

resource "azurerm_role_assignment" "prometheus-sd-user-reader" {
  scope                = data.azurerm_subscription.current.id
  principal_id         = azuread_service_principal.prometheus-sd-user.id
  role_definition_name =  "Reader"
}

output "azure_subscription_id" {
  value = data.azurerm_subscription.current.id
}

output "azure_tenant_id" {
  value = data.azurerm_subscription.current.tenant_id
}

output "oauth_client_id" {
  value = azuread_application.prometheus-sd-user.application_id
}
