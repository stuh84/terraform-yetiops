# Openstack Provider
provider "openstack" {
  user_name   = "admin"
  auth_url    = "http://10.20.20.1:5000/v3"
  region      = "microstack"
  user_id     = "$USERID"
  application_credential_id = "$APP_CRED_ID"
  application_credential_secret = "$APP_CRED_SECRET"
  tenant_id   = "$TENANT_ID"
}
